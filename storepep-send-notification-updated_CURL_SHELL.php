<?php
try {
    if (isset($_GET['type']) && isset($_GET['shop_url'])) {
        $shop_url = $_GET['shop_url'];
        $typeOfNotification = $_GET['type'];
        $urlToCloneStoringdevice="";
        switch ($typeOfNotification) {
            case "new_order":
                $currency = $_GET['currency'];
                $total = $_GET['total'];
                $id = $_GET['id'];
		        $payment = $_GET['payment'];
                $payment = rawurlencode($payment);      
                if(isset($_GET['date_created'])){
                    $date_created = $_GET['date_created'];
                    $urlToCloneStoringdevice = "https://app-notify.storepep.com/storepep-send-notification-updated.php?shop_url=".$shop_url."&type=".$typeOfNotification."&id=".$id."&currency=".$currency."&total=".$total."&payment=".$payment."&date_created=".$date_created;
                } else {
                    $urlToCloneStoringdevice = "https://app-notify.storepep.com/storepep-send-notification-updated.php?shop_url=".$shop_url."&type=".$typeOfNotification."&id=".$id."&currency=".$currency."&total=".$total."&payment=".$payment;
                }
                break;
            case "new_customer":
                $username = $_GET['username'];
                $username=rawurlencode($username);                
                $name = $_GET['name'];
                $name=rawurlencode($name);               
                $id = $_GET['id'];
                $urlToCloneStoringdevice = "https://app-notify.storepep.com/storepep-send-notification-updated.php?shop_url=".$shop_url."&type=".$typeOfNotification."&id=".$id."&username=".$username."&name=".$name;
                break;
            case "low_stock":
                $name = $_GET['name']; 
                $name=rawurlencode($name);               
                $id = $_GET['id'];
                $quantity = $_GET['quantity'];
                $urlToCloneStoringdevice = "https://app-notify.storepep.com/storepep-send-notification-updated.php?shop_url=".$shop_url."&type=".$typeOfNotification."&id=".$id."&name=".$name."&quantity=".$quantity;
                break;
	    case "no_stock":
                $name = $_GET['name'];
                $name=rawurlencode($name);              
                $id = $_GET['id'];
                $quantity = $_GET['quantity'];
                $urlToCloneStoringdevice = "https://app-notify.storepep.com/storepep-send-notification-updated.php?shop_url=".$shop_url."&type=".$typeOfNotification."&id=".$id."&name=".$name."&quantity=".$quantity;
                break;
            case "order_status":
                $from = $_GET['from'];    
                $from = rawurlencode($from);
                $id = $_GET['id'];
                $to = $_GET['to'];
                $to=rawurlencode($to);  
                if(isset($_GET['date_created'])){
                    $date_created = $_GET['date_created'];
                    $urlToCloneStoringdevice = "https://app-notify.storepep.com/storepep-send-notification-updated.php?shop_url=".$shop_url."&type=".$typeOfNotification."&id=".$id."&from=".$from."&to=".$to."&date_created=".$date_created;
                } else {
                    $urlToCloneStoringdevice = "https://app-notify.storepep.com/storepep-send-notification-updated.php?shop_url=".$shop_url."&type=".$typeOfNotification."&id=".$id."&from=".$from."&to=".$to;
                }             
                break;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $urlToCloneStoringdevice);
        $result = curl_exec($ch);
    } else {
        echo "Invalid Parameters!";
    }
} catch (Exception $e) {
    echo "Error - " . $e->getMessage();
}
?>

